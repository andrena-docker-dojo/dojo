# Andrena Docker Dojo

## Vorbereitung

Docker installieren
Java 11 installieren

Optional: User zur Dockergroup hinzufügen

https://www.digitalocean.com/community/tutorials/how-to-set-up-a-private-docker-registry-on-ubuntu-20-04
https://stnimmerlein.de
andrena dojo

### (Unvollständige) Befehlssammlung
- `docker run [--name CONTAINERNAME] IMAGE` - Erstelle neuen Container [mit namen] von Image
- `docker start CONTAINERID` - Starte gestoppten Container
- `docker stop CONTAINERID` - Stoppe laufenden Container
- `docker kill CONTAINERID` - Beende laufenden Container forcefully
- `docker logs [-f] CONTAINERID` - Zeige Logs des Containers [als Stream]
- `docker ps [-a]` - Zeige die laufenden [bzw. alle] Container
- `docker rm CONTAINERID` - Lösche einen gestoppten Container
- `docker images` - Zeige die Images in der lokalen Registry
- `docker rmi IMAGEID` - Lösche ein Image aus der lokalen Registry
- `docker exec -it CONTAINERID COMMAND` - Führe den Befehl `COMMAND` auf dem Container interaktiv aus. Vor allem häufig dafür genutzt:
- `docker exec -it CONTAINERID bash` - Starte ein interaktives Terminal innerhalb des Containers. Ist kein `bash` vorhanden, kann man es mit `sh` versuchen.
- `docker cp CONTAINERID:/some/file /some/local/path` - Kopiere eine Datei aus dem Container aufs lokale Dateisystem
- `docker cp /some/file CONTAINERID:/some/path` - Kopiere eine Datei vom lokalen Dateisystem in den Container
- `docker commit CONTAINERID IMAGENAME:IMAGETAG` - Erstelle ein neues Image von aktuellen Stand des Containers

## Aufgaben

### 1: Anfang

Starte einen einfachen Docker Container:
```bash
docker run registry.gitlab.com/andrena-docker-dojo/hello-world:latest
```

Der Output wurde innerhalb des Containers generiert. Anschließend hat der Container sein Werk vollbracht und hat sich automatisch beendet.

### 2: Die CI - Grundlagen

Betrachte laufende Container mit `docker ps`, alle Container (auch die gestoppten) mit `docker ps -a`.
Finde hier den gerade eben erstellten Container und starte ihn erneut: `docker start CONTAINER_ID`.
Die Container-Id muss nur soweit ausgeschrieben werden, bis sie eindeutig ist.

Warum läuft der Container immer noch nicht? Warum bekommen wir diesmal keinen Log?

Starte den Container erneut, diesmal mit `docker start CONTAINER_ID -i`. Nun sehen wir den Log, da wir ihn im interaktiven Modus gestartet haben.

Erzeuge einen neuen Container wie oben (mit `docker run`), diesmal aber inkl. `-d`. Diesmal erscheint auch kein log, da wir den Container im Daemon-Mode gestartet haben.
Merke: `docker run` erstellt einen Container und startet ihn per Default interaktiv, `docker start` hingegen startet einen bestehenden, getoppten Container per Default im Daemon-Mode.

Betrachte die Logs des Dockers, also den Output: `docker logs CONTAINER_ID`.

Lösche nicht mehr benötigte Container: `docker rm CONTAINER_ID`.

Betrachte die Images in der lokalen Registry: `docker images`
Lösche nicht mehr benötigtes Image: `docker rmi IMAGE_ID`.
Auch hier muss die Image-Id lediglich eindeutig sein und nicht komplett ausgeschrieben werden.

Lade wieder ein Image in die lokale registry, ohne daraus einen Container zu bauen: `docker pull registry.gitlab.com/andrena-docker-dojo/hello-world:latest`

Validiere dies mit `docker ps -a` und `docker images`.

### 3: Jetzt bringen wir Sinn in die Sache

Starte einen Container: `docker run nginx`
Dies startet einen Container mit einem Webserver (wie man an den logs vielleicht sieht). Validiere in einem neuen Terminal, dass der container auch gerade läuft.
Beende die Logs (Ctrl + C) - der Container wird beendet. Mist. Starte den Container wieder `docker start`. Jetzt läuft er, ohne zu blockieren. Stoppe ihn (`docker stop`) und erzeuge einen neuen Container, diesmal mit `-d`. Diesmal blockiert er schon direkt von Anfang an aus nicht (da er direkt im Daemon-mode gestartet wurde).

Der Webserver ist aber nicht erreichbar, dazu brauchen wir Portweiterleitung vom Host in den Container. Dazu muss der Container neu angelegt werden (ein bestehender Container lässt sich leider nicht mehr anpassen): `docker run -d -p 90:80 nginx`
Das mappt den Port 90 des Hosts auf Port 80 im Container. Nginx hört auf Port 80, also müssen wir auf diesen Mappen. Mehrere Ports (z.B. noch für https) können in Reihe angegeben werden: `-p 90:80 -p 443:443` (in diesem Fall wird 443 des Hosts auf 443 des Containers gemappt - das ist natürlich auch möglich).

Prüft, dass der Container auch läuft. Dann öffnet euren Webbrowser und öffnet `http://localhost:90`. Ihr solltet die nginx-Defaultseite sehen. Wunderbar.
Verwendet ihr stattdessen `https`, dann gibt es einen Fehler (weil unser Server nur http ausspielt). Der Fehler wird auch im Container gelogt - testet das. Um die logs offen zu halten, fügt bei `docker logs` noch die flag `-f` hinzu, dann wird der log live gestreamt.

Springt in den laufenden Container: `docker exec -it CONTAINER_ID bash` - das startet im Container eine bash (ein Terminal) und stellt es euch direkt interaktiv zur Verfügung.
Hier könnt ihr euch z.B. die nginx config angucken (`cat /etc/nginx/conf.d/default.conf`)
Man sieht, dass das Webroot in `/usr/share/nginx/html` liegt. Fügt am Dateiende einen Text hinzu, um zu sehen, dass das auch tatsächlich eine Auswirkung auf die Webseite hat: `echo "EUER TEXT" >> /usr/share/nginx/html/index.html`, dann ladet die Seite im Webbrowsser neu (evtl. Cache leeren).

Natürlich ist es sehr umständlich, im Container selbst die Dateien zu haben. Und wenn man den Container durch einen neuen (z.B. mit einer neueren nginx-Version) Updaten will, sollen die Daten (in diesem Fall die Webseite) ja erhalten bleiben. Sinnvoller wäre es also, den Webroot auf dem Host zu haben und nur mit dem Container zu verknüpfen.
Also: Legt einen Ordner im Host an (z.B. ~/meineWebseite) und legt dort eine Datei `index.html` mit irgendeinem Dummy-Content an.

Dann stopen wir den laufenden Container und erzeugen einen neuen (auch hier können wir den bereits existierenden Container nicht mehr anpassen): `docker run -d -p 90:80 -v ~/meineWebseite:/usr/share/nginx/html nginx`
Ruft wieder `http://localhost:90` auf und tada - man sieht die eigene index.html. Auch hier können bei Bedarf mehrere Volumes nacheinander angegeben werden (`-v /some:/vol -v /someOther:/otherVol`), z.B. noch für die Config-Files etc.

Jetzt habt ihr bereits gelernt, wie ihr einen nginx-Webserver in Docker starten könnt, mit eigenen Daten befüllen und das, ganz ohne auf der Host Machine mehr einzurichten als lediglich Docker (insbes. kein nginx).

### 4: Eigene Images anlegen

Wenn ihr ein eigenes Image anlegen wollt (z.B. eine coole Webapp geschrieben habt, die ganz einfach installiert und gestartet werden soll, indem man einen Docker container startet), dann geht das natürlich auch.

Klont as Project https://gitlab.com/andrena-docker-dojo/java-web-app.git . Ihr könnt es erst ohne Docker ausprobieren, indem ihr es baut (`./gradlew build`) und anschließend ausführt (`java -jar build/libs/java-web-app-0.0.1-SNAPSHOT.jar`). Besucht dann im Webbrowser `http://localhost:8080`. Wenn ihr hier einen Text seht, funktioniert die WebApp prinzipiell.
Falls ihr kein Java installiert habt oder Probleme beim Bauen habt, könnt ihr diesen Schritt auch überspringen, euch die fertige `.jar`-Datei von einem Kollegen geben lassen und mit Teil a weitermachen.

#### a: Container zum Ausführen

Nun wollen wir einen Dockercontainer erstellen (bzw. erstmal ein Image), welcher diese WebApp ausführt, damit auf der Hostmachine z.B. kein Java installiert sein muss.

Das machen wir über ein Dockerfile:

Legt einen neuen Order an, kopiert die vorher kompilierte Datei `java-web-app-0.0.1-SNAPSHOT.jar` dort hinein und legt eine neue Textdatei namens `Dockerfile` an, mit folgendem Inhalt:

```Dockerfile
FROM openjdk:11-jre-slim-buster

COPY java-web-app-0.0.1-SNAPSHOT.jar /app/app.jar

CMD java -jar /app/app.jar
```

Wir verwenden `openjdk:11-jre-slim-buster` als Basisimage. Dann kopieren wir die im selben Ordner liegende Datei in den Container, und zwar nach `/app/app.jar`.
Zum Schluss sagen wir dem Image, dass jeder Container, der daraus gebaut wird, als Startbefehl dieses jar ausführen soll.

Jetzt können wir das Image bauen. Navigiert im Terminal in den selben Ordner, in dem auch das `Dockerfile` liegt. Anschließend: `docker build -t dojo:webapp .`. Der Parameter hinter `-t` ist der Name des Images inklusive seines Tags. So könnten wir verschiedene Versionen anlegen, `dojo:1`, `dojo:2` etc. oder auch, wie bei beim openjdk-Basis-Image zum Beispiel, verschiedene Varianten (JDK, JRE, Java 8, Java 11, ...).

Das Image wurde erstellt (`docker images`) und kann als Container gestartet werden. Da die Webapp innerhalb des Containers unter Port 8080 erreichbar sein wird, wir sie vielleicht im Host aber unter 9090 aufrufen wollen, verwenden wir: `docker run -p 9090:8080 -d dojo:webapp`.

Wunderbar, es sollte alles geklappt haben.

Die Webapp hat neben dem URL-Pfad `/` auch noch `/crash` implementiert; wenn ihr `localhost:9090/crash` aufruft, stürzt die App ab. Tut dies und guckt, was mit dem Container selbst passiert.
Können wir den Container so starten, dass er bei einem Crash einfach neustartet? Guckt euch bei `docker run` den Parameter `--restart` an und probiert es damit. Testet es, indem ihr den Container startet, zum Crashen bringt und schaut, ob er wieder neu startet.

#### b: Container zum Bauen und Ausführen

Nun wollen wir uns das manuelle Klonen und Bauen der App auch sparen. Im Dockerfile kann man mit `RUN` Befehle angeben, die beim Bauen des Images einmalig ausgeführt werden (z.B. `RUN cd /app && ./gradlew build`).

Jetzt wollen wir also ein Dockerfile anlegen, welches selbstständig das Repo klont, baut und beim Containerstart weiterhin die gebaute App startet. Hier müsst ihr auch ein anderes Basisimage verwenden, denn in `openjdk:11-jre-slim-buster` ist nur die JRE, nicht das JDK, enthalten. Guckt einfach mal auf `https://hub.docker.com/_/openjdk/tags`, ob es hier vielleicht etwas passendes für euch gibt. Zusätzlich kann fehlende Software auch ganz normal in einem `RUN` installiert werden (z.B. `RUN apt-get update && apt-get install git -y`).

Wenn ihr alles hinbekommen habt, dann könnt ihr damit nun ein Image erzeugen (nennt es `-t dojo:webappwithbuild`), welches die (zu diesem Zeitpunkt) neueste Version in gebauter Form beinhaltet - ganz ohne selbst etwas kompilieren zu müssen oder gar Java installiert zu haben.

Was ist das Problem damit? Vergleicht die Größe der Images `dojo:webapp` und `dojo:webappwithbuild` sowie die Größe der Container, die dadurch erstellt wurden (`docker images` bzw. `docker ps -a --size`). Das sollte ein Hinweis darauf sein, warum diese Lösung vielleicht nicht die beste ist.

#### c: Ein Container zum Bauen, einer zum Ausführen

Wie lösen wir das Problem? Wir brauchen zwei Container - einen zum Bauen, einen zum Ausliefern der App. Der Build-Container zieht (optimalerweise bei jedem neuen start) die neueste Version von git, baut sie, stellt das jar nach außen zur Verfügung und beendet sich wieder.
Statt, dass ihr alles als Einzeiler als `CMD` schreibt, legt dazu vielleicht ein bash-Script an, welches dann in `CMD` aufgerufen wird.
Dann können wir, wie in Teil a, einen Container bauen, der das gebaute Artefakt nimmt und intern ausführt.

Baut entsprechende Container-Images mit Dockerfiles.

#### d: Man kann ein Image auch aus einem bestehenden Container heraus anlegen

Betrachtet das Projekt, mit welchem das Hello-World-Image vom Anfang gebaut wurde: https://gitlab.com/andrena-docker-dojo/hello-world. Man sieht, dass der Container letztendlich einfach ein Bash-Script ausführt.
Sucht den zugehörigen Container in euren lokalen Containern (`docker ps -a`) (oder, wenn ihr ihn gelöscht habt, erstellt ihn nochmal neu: `docker run registry.gitlab.com/andrena-docker-dojo/hello-world:latest`) und merkt euch die ID.
Als nächstes kopieren wir die Datei `run.sh` aus dem Container auf unseren Host: `docker cp CONTAINERID:/run.sh .`
Bearbeitet die Datei nach Belieben. Anschließend kopieren wir sie wieder in den Container: `docker cp run.sh CONTAINERID:/run.sh`.
Testet, dass die Änderung geklappt hat und startet den Container (und schaut in seine Logs).
Nun wollen wir ein Image ausgehend von diesem Container anlegen: `docker commit CONTAINERID IMAGENAME:IMAGETAG`.
Testet es, indem ihr nun aus dem neuen Image einen neuen Container erstellen lasst (mit `docker run`).


### 5 Images in Registry pushen und updaten

Nun habt ihr die Images lokal auf euren Rechnern, aber was, wenn ihr sie mit anderen Leuten (z.B. euren Teamkollegen) teilen wollt? Mit `docker save` können Images als `tar`-Archiv gespeichert werden, das könnte man dann verschicken. Ist aber in aller Regel nicht der komfortabelste und beste Weg.
Stattessen können wir unsere Images in eine Registry pushen, von der dann jeder andere sie wieder herunterladen kann.
Es gibt öffentliche Registries (wie zum Beispiel docker hub, die Standardregistry), von der jeder Images herunterladen kann; zum Hochladen wird ein Account benötigt. Allerdings möchte man ja in der Regel seine eigene Registry haben, um dort nur mit dem Projektteam die Container zu teilen. Dann benötigt man auch zum Herunterladen ggf. schon einen Login.

Ich habe für das Dojo eine kleine Docker Registry unter `https://stnimmerlein.de` eingerichtet. Dorthin wollen wir nun unser angepasstes Image aus 4.d pushen. Dazu wie folgt vorgehen:
Erst loggen wir uns in der Registry ein: `docker login stnimmerlein.de` (Benutzername ist `andrena`, Passwort ist `dojo`). Nun müssen wir unserem Image noch einen neuen Namen geben. Images, die in einer anderen Registry als Docker hub liegen sollen, müssen diese Registry im Namen tragen.
Sucht mit `docker images` die ID des Images aus 4.d und gebt ihm einen zusätzlichen Namen: `docker tag IMAGEID stnimmerlein.de/NEWNAME:NEWTAG`
Nun können wir das Image in die Registry pushen: `docker push stnimmerlein.de/NEWNAME:NEWTAG`.

Zum Testen könnt ihr nun mal das Image eines anderen Kollegen aus der Registry pullen und ausführen: `docker run stnimmerlein.de/OTHER_IMAGE_NAME:TAG`.

Wenn der Kollege nun eine neue Version seines Images in die Registry pusht, mit selbem Tag, und ihr wieder den `docker run` Befehl ausführt, wird Docker die lokal gespeicherte Kopie des Images verwenden und daher nicht die neue Version von der Registry laden. Mit `docker pull IMAGE` könnt ihr die lokale Kopie durch die aktuelle vom Server erstetzen.

### 6 Der latest-Tag

Hier erzähle ich nur was kleines.

### 7 Image-Größe

Optimalerweise wollen wir Images so klein wie möglich halten.
Das heißt einerseits, dass in einem Container auch nur das installiert sein sollte, was benötigt wird. Wenn ich nur einen Text ausgebe, dann brauche ich kein Java. Daher erbt das Hello-World-Image auch nur von `alpine` (einem sehr sehr kleinen System) und nicht von beispielsweise `openjdk:11-jre-slim-buster`. Das hält sowohl das Image als auch den erzeugten Container klein.

Es gibt aber noch einen zweiten Faktor, der einen Einfluss auf die Image-Größe hat:
Erstellt ein Dockerfile:
```
FROM alpine

CMD echo "Ich bin gestartet"
```
Legt daraus ein Image an, mit dem Namen `sizecomparison:0`.

Nun passt das Dockerfile so an:
```
FROM alpine

RUN dd if=/dev/urandom of=/largeFile.txt bs=500MB count=1
RUN rm /largeFile.txt

CMD echo "Ich bin gestartet"
```
Hier wird zuerst eine Datei mit einer Größe von 500MB angelegt und anschließend wieder gelöscht. Legt ein neues Image mit diesem Dockerfile an, mit Namen `sizecomparison:1`.

Nun legt ein drittes Image mit Namen `sizecomparison:2` aus folgendem Dockerfile an:
```
FROM alpine

RUN dd if=/dev/urandom of=/largeFile.txt bs=500MB count=1 && rm /largeFile.txt

CMD echo "Ich bin gestartet"
```
Hier erstellen und löschen wir die Datei im selben RUN-Befehl.

Vergleicht die Größen der einzelnen Images: `docker images`
Ihr könnt auch mit den einzelnen Images Container starten (`docker run`) und deren Größen vergleichen: `docker ps -a --size`.

Jetzt kommt eine kurze mündliche Erklärung ;-)
Zusammenfassung: https://vsupalov.com/docker-image-layers/#:~:text=Each%20layer%2C%20is%20a%20complete,%2Dfriendly%20name%3Atag%20pair.
`docker image inspect IMAGEID`

### 8 Security

Hier erklärt Paul etwas.


